# Weather-website

Here is the source code for the weather website I realized according to [The Complete Node.js Developer Course (3rd Edition) on Udemy](https://www.udemy.com/course/the-complete-nodejs-developer-course-2/). 
It was realized with HTML, CSS, JavaScript and Node.js.

You can visit the webpage on https://mahaffne-weather-app.herokuapp.com/ .

Navigate through the `/help` to get the usage of the application.

You can find more details about my resume on the `/about` page.

Enjoy !

